# leetcode-solutions

My solutions for LC problems:

1. [Two Sum](https://leetcode.com/problems/two-sum/)
2. [Reverse Linked List](https://leetcode.com/problems/reverse-linked-list/)
3. [Merge Two Sorted Lists](https://leetcode.com/problems/merge-two-sorted-lists/)
4. [String to Integer (atoi)](https://leetcode.com/problems/string-to-integer-atoi/)
5. [Add Two Numbers](https://leetcode.com/problems/add-two-numbers/)
6. [Add Two Numbers II](https://leetcode.com/problems/add-two-numbers-ii/)
7. [Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/)
8. [Best Time to Buy and Sell Stock II](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/)
9. [Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/)
10. [Subarray Sum Equals K](https://leetcode.com/problems/subarray-sum-equals-k/)
11. [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/)
12. [Remove Duplicates from Sorted Array](https://leetcode.com/problems/remove-duplicates-from-sorted-array/)
13. [Roman to Integer](https://leetcode.com/problems/roman-to-integer/)
14. [Longest Palindromic Substring](https://leetcode.com/problems/longest-palindromic-substring/)
15. [Longest Common Prefix](https://leetcode.com/problems/longest-common-prefix/)
16. [ZigZag Conversion](https://leetcode.com/problems/zigzag-conversion/)
17. [Implement strStr()](https://leetcode.com/problems/implement-strstr/)
18. [Median of Two Sorted Arrays](https://leetcode.com/problems/median-of-two-sorted-arrays/)
