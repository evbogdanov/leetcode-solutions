const solution1 = require("./solution1");
const solution2 = require("./solution2");

describe("Two Sum", () => {
  const nums = [2, 7, 11, 15];
  const target = 9;
  const expected = [0, 1];

  test("First solution", () => {
    expect(solution1(nums, target)).toEqual(expected);
  });

  test("Second solution", () => {
    expect(solution2(nums, target)).toEqual(expected);
  });

  test("No solution", () => {
    expect(() => solution1(nums, 1)).toThrow();
    expect(() => solution2(nums, 1)).toThrow();
  });
});
