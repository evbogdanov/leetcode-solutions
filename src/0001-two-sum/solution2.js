/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
function twoSum(nums, target) {
  const seenNums = new Map();

  for (let i = 0; i < nums.length; i += 1) {
    const currentNum = nums[i];
    const wantedNum = target - currentNum;
    const maybeFoundIndex = seenNums.get(wantedNum);

    if (maybeFoundIndex !== undefined) {
      return [maybeFoundIndex, i];
    }

    seenNums.set(currentNum, i);
  }

  throw new Error("No solution");
}

module.exports = twoSum;
