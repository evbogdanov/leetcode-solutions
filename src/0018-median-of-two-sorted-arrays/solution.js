function mergeSortedArrays(nums1, nums2) {
  const result = [];

  while (nums1.length && nums2.length) {
    const num1 = nums1[0];
    const num2 = nums2[0];

    if (num1 < num2) {
      result.push(num1);
      nums1.shift();
    } else {
      result.push(num2);
      nums2.shift();
    }
  }

  if (nums1.length) {
    result.push(...nums1);
  } else if (nums2.length) {
    result.push(...nums2);
  }

  return result;
}

/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
function findMedianSortedArrays(nums1, nums2) {
  const nums = mergeSortedArrays(nums1, nums2);
  const len = nums.length;

  if (len === 0) return null;

  if (len === 1) return nums[0];

  const middle = Math.floor(len / 2);
  if (len % 2 === 0) {
    const right = middle;
    const left = right - 1;
    return (nums[left] + nums[right]) / 2;
  }

  return nums[middle];
}

module.exports = findMedianSortedArrays;
