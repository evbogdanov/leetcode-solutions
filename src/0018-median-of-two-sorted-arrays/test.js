const solution = require("./solution");

const INPUT_OUTPUT = [
  { input: [[], []], output: null },
  { input: [[], [1]], output: 1 },
  {
    input: [
      [0, 0],
      [0, 0],
    ],
    output: 0,
  },
  { input: [[1, 3], [2]], output: 2 },
  {
    input: [
      [1, 2],
      [3, 4],
    ],
    output: 2.5,
  },
];

describe("Median of Two Sorted Arrays", () => {
  test("Solution", () => {
    for (const { input, output } of INPUT_OUTPUT) {
      expect(solution(input[0], input[1])).toBe(output);
    }
  });
});
