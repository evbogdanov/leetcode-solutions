const { INT32_MIN, INT32_MAX } = require("../shared/constants");

/**
 * @param {string} str
 * @return {number}
 */
function myAtoi(str) {
  const re = /^\s*(?<sign>[-+]?)(?<num>\d+)/;
  const found = str.match(re);

  if (!found) return 0;

  const sign = found.groups.sign === "-" ? -1 : 1;
  const num = sign * found.groups.num;

  if (num < INT32_MIN) return INT32_MIN;
  if (num > INT32_MAX) return INT32_MAX;

  return num;
}

module.exports = myAtoi;
