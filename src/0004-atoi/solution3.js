const { INT32_MIN, INT32_MAX } = require("../shared/constants");

/**
 * @param {string} str
 * @return {number}
 */
function myAtoi(str) {
  const num = parseInt(str, 10);

  if (!num) return 0;

  if (num < INT32_MIN) return INT32_MIN;
  if (num > INT32_MAX) return INT32_MAX;

  return num;
}

module.exports = myAtoi;
