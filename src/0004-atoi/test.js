const { INT32_MIN } = require("../shared/constants");

const solution1 = require("./solution1");
const solution2 = require("./solution2");
const solution3 = require("./solution3");

const INPUT_OUTPUT = [
  ["", 0],
  ["42", 42],
  ["0xF", 0],
  ["   -42", -42],
  ["   +42", 42],
  ["--42", 0],
  ["42 with words", 42],
  ["words and 42", 0],
  ["00000-42a1234", 0],
  ["-91283472332", INT32_MIN],
];

describe("String to Integer (atoi)", () => {
  const testSolution = (solution) => {
    for (const [input, output] of INPUT_OUTPUT) {
      expect(solution(input)).toBe(output);
    }
  };

  test("Solution1", () => {
    testSolution(solution1);
  });

  test("Solution2", () => {
    testSolution(solution2);
  });

  test("Solution3", () => {
    testSolution(solution3);
  });
});
