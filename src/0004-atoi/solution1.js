const { INT32_MIN, INT32_MAX } = require("../shared/constants");

/**
 * @param {string} str
 * @return {number}
 */
function myAtoi(str) {
  let isParsingLeadingWhitespace = true;
  let isParsingSign = false;
  let sign = 1;
  let digits = "";

  for (let i = 0; i < str.length; i += 1) {
    const ch = str[i];

    if (isParsingLeadingWhitespace && ch === " ") continue;

    if (isParsingLeadingWhitespace && ch !== " ") {
      isParsingLeadingWhitespace = false;
      isParsingSign = true;
    }

    if (isParsingSign) {
      isParsingSign = false;

      if (ch === "-" || ch === "+") {
        sign = ch === "-" ? -1 : 1;
        continue;
      }
    }

    const isNumber =
      ch === "0" ||
      ch === "1" ||
      ch === "2" ||
      ch === "3" ||
      ch === "4" ||
      ch === "5" ||
      ch === "6" ||
      ch === "7" ||
      ch === "8" ||
      ch === "9";

    if (!isNumber) break;

    digits += ch;
  }

  if (digits === "") return 0;

  const num = sign * digits;

  if (num < INT32_MIN) return INT32_MIN;
  if (num > INT32_MAX) return INT32_MAX;

  return num;
}

module.exports = myAtoi;
