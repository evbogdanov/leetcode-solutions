/**
 * @param {string} str
 * @param {number} numRows
 * @return {string}
 */
function convert(str, numRows) {
  // Indices of rows in a zigzag pattern
  const indices = [];

  // First part of the pattern: column
  for (let i = 0; i < numRows; i += 1) {
    indices.push(i);
  }

  // Second part of the pattern: diagonal
  for (let i = numRows - 2; i >= 1 && i < numRows; i -= 1) {
    indices.push(i);
  }

  const rows = new Array(numRows).fill("");

  for (
    let strInd = 0, ind = 0;
    strInd < str.length;
    strInd += 1, ind = ind === indices.length - 1 ? 0 : ind + 1
  ) {
    const ch = str[strInd];
    const index = indices[ind];

    rows[index] += ch;
  }

  return rows.join("");
}

module.exports = convert;
