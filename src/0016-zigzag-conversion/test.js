const solution = require("./solution");

describe("ZigZag Conversion", () => {
  test("Solution", () => {
    expect(solution("A", 1)).toBe("A");
    expect(solution("PAYPALISHIRING", 3)).toBe("PAHNAPLSIIGYIR");
    expect(solution("PAYPALISHIRING", 4)).toBe("PINALSIGYAHRPI");
  });
});
