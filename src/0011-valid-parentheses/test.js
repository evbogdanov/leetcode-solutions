const solution = require("./solution");

const INPUT_OUTPUT = [
  ["()", true],
  ["(]", false],
  ["(])", false],
  ["()[]{}", true],
  ["([)]", false],
  ["{[]}", true],
];

describe("Valid Parentheses", () => {
  test("Solution", () => {
    for (const [input, output] of INPUT_OUTPUT) {
      expect(solution(input)).toBe(output);
    }
  });
});
