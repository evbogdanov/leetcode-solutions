const BRACKETS = {
  "(": ")",
  "[": "]",
  "{": "}",
};

/**
 * @param {string} s
 * @return {boolean}
 */
function isValid(str) {
  const openBrackets = [];

  for (const currentBracket of str) {
    if (BRACKETS.hasOwnProperty(currentBracket)) {
      openBrackets.push(currentBracket);
      continue;
    }

    const bracketToClose = openBrackets[openBrackets.length - 1];

    // No open brackets seen before. Or current bracket closes wrong open bracket.
    if (!bracketToClose || currentBracket !== BRACKETS[bracketToClose]) {
      return false;
    }

    openBrackets.pop();
  }

  return openBrackets.length === 0;
}

module.exports = isValid;
