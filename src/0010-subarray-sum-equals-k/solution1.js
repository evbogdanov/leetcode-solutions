/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
function subarraySum(nums, k) {
  let counter = 0;

  for (let i = 0; i < nums.length; i += 1) {
    let sum = 0;

    for (let j = i; j < nums.length; j += 1) {
      sum += nums[j];
      if (sum === k) {
        counter += 1;
      }
    }
  }

  return counter;
}

module.exports = subarraySum;
