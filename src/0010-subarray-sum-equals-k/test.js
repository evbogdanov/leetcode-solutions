const solution1 = require("./solution1");

const INPUT_OUTPUT = [
  { input: [[1], 0], output: 0 },
  { input: [[-1, -1, 1], 0], output: 1 },
  { input: [[1, 1, 1], 2], output: 2 },
  { input: [[1, 2, 3], 3], output: 2 },
];

describe("Subarray Sum Equals K", () => {
  const testSolution = (solution) => {
    for (const { input, output } of INPUT_OUTPUT) {
      expect(solution.apply(null, input)).toBe(output);
    }
  };

  test("Solution1", () => {
    testSolution(solution1);
  });
});
