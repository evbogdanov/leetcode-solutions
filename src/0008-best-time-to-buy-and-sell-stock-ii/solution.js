function maxProfit(prices) {
  let maxProf = 0;
  let savedBuyPrice = null;

  for (let i = 0; i < prices.length; i += 1) {
    const currentPrice = prices[i];
    const nextPrice = prices[i + 1] || -Infinity;

    // 1. No bought stocks yet. I'm looking for the best price to buy.
    if (savedBuyPrice === null) {
      if (currentPrice < nextPrice) {
        savedBuyPrice = currentPrice;
      }
      // If currentPrice >= nextPrice, just skip it for now.

      continue;
    }

    // 2. I've already bought a stock. It's time to find the right price to sell.
    if (currentPrice > nextPrice) {
      maxProf += currentPrice - savedBuyPrice;
      savedBuyPrice = null;
    }
    // If currentPrice <= nextPrice, then wait.
  }

  return maxProf;
}

module.exports = maxProfit;
