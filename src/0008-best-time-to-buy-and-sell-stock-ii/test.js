const solution = require("./solution");

const INPUT_OUTPUT = [
  [[], 0],
  [[1], 0],
  [[1, 1], 0],
  [[1, 2], 1],
  [[1, 2, 3], 2],
  [[3, 2, 1], 0],
  [[4, 3, 2, 1], 0],
  [[2, 1, 2, 0, 1], 2],
  [[7, 1, 5, 3, 6, 4], 7],
  [[1, 2, 3, 4, 5], 4],
];

describe("Best Time to Buy and Sell Stock II", () => {
  test("Solution", () => {
    for (const [input, output] of INPUT_OUTPUT) {
      expect(solution(input)).toBe(output);
    }
  });
});
