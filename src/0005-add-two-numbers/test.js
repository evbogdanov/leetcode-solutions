const ListNode = require("../shared/ListNode");

const solution = require("./solution");

describe("Add Two Numbers", () => {
  test("Solution", () => {
    const list1Node3 = new ListNode(9, null);
    const list1Node2 = new ListNode(9, list1Node3);
    const list1 = new ListNode(9, list1Node2);

    const list2Node2 = new ListNode(1, null);
    const list2 = new ListNode(1, list2Node2);

    const result = solution(list1, list2);

    // 11 + 999 = 1010
    // (reversed: 0101)
    expect(result.val).toBe(0);
    expect(result.next.val).toBe(1);
    expect(result.next.next.val).toBe(0);
    expect(result.next.next.next.val).toBe(1);
    expect(result.next.next.next.next).toBeNull();
  });
});
