const ListNode = require("../shared/ListNode");

/**
 * @param {ListNode} ptr1
 * @param {ListNode} ptr2
 * @return {ListNode}
 */
function addTwoNumbers(ptr1, ptr2) {
  const resultPtrStart = new ListNode("dummy", null);
  let resultPtrEnd = resultPtrStart;
  let savedDigit = 0;

  while (true) {
    const isPtr1Null = ptr1 === null;
    const isPtr2Null = ptr2 === null;

    if (isPtr1Null && isPtr2Null) {
      if (savedDigit) {
        const lastNode = new ListNode(savedDigit, null);
        resultPtrEnd.next = lastNode;
      }
      break;
    }

    const val1 = isPtr1Null ? 0 : ptr1.val;
    const val2 = isPtr2Null ? 0 : ptr2.val;
    const sum = val1 + val2 + savedDigit;
    const digit = sum % 10;
    savedDigit = sum >= 10 ? 1 : 0;

    const node = new ListNode(digit, null);
    resultPtrEnd.next = node;
    resultPtrEnd = node;

    if (!isPtr1Null) ptr1 = ptr1.next;
    if (!isPtr2Null) ptr2 = ptr2.next;
  }

  return resultPtrStart.next;
}

module.exports = addTwoNumbers;
