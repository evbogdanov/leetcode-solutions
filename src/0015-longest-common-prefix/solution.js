/**
 * @param {string[]} strs
 * @return {string}
 */
function longestCommonPrefix(strs) {
  if (strs.length === 0) return "";

  const firstStr = strs[0];
  let lcp = "";

  for (let i = 0; i < firstStr.length; i += 1) {
    const ch = firstStr[i];
    for (let j = 1; j < strs.length; j += 1) {
      if (strs[j][i] !== ch) return lcp;
    }
    lcp += ch;
  }

  return lcp;
}

module.exports = longestCommonPrefix;
