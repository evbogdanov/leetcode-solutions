const solution = require("./solution");

describe("Longest Common Prefix", () => {
  test("Solution", () => {
    expect(solution(["flower", "flow", "flight"])).toBe("fl");
    expect(solution(["dog", "racecar", "car"])).toBe("");
  });
});
