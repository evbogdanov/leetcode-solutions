const ListNode = require("../shared/ListNode");

const solution1 = require("./solution1");
const solution2 = require("./solution2");

describe("Add Two Numbers II", () => {
  test("Add zeros", () => {
    const list1 = new ListNode(0, null);
    const list2 = new ListNode(0, null);

    const result1 = solution1(list1, list2);
    expect(result1.val).toBe(0);
    expect(result1.next).toBeNull();

    const result2 = solution2(list1, list2);
    expect(result2.val).toBe(0);
    expect(result2.next).toBeNull();
  });

  test("Add numbers", () => {
    const list1Node3 = new ListNode(9, null);
    const list1Node2 = new ListNode(9, list1Node3);
    const list1 = new ListNode(9, list1Node2);

    const list2Node2 = new ListNode(1, null);
    const list2 = new ListNode(1, list2Node2);

    const result1 = solution1(list1, list2);
    expect(result1.val).toBe(1);
    expect(result1.next.val).toBe(0);
    expect(result1.next.next.val).toBe(1);
    expect(result1.next.next.next.val).toBe(0);
    expect(result1.next.next.next.next).toBeNull();

    const result2 = solution2(list1, list2);
    expect(result2.val).toBe(1);
    expect(result2.next.val).toBe(0);
    expect(result2.next.next.val).toBe(1);
    expect(result2.next.next.next.val).toBe(0);
    expect(result2.next.next.next.next).toBeNull();
  });
});
