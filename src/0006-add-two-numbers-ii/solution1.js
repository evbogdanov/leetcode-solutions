// This solution fails with big integers because:
// https://stackoverflow.com/questions/13429451/why-is-9999999999999999-converted-to-10000000000000000-in-javascript

const ListNode = require("../shared/ListNode");

function listToNumber(ptr) {
  let numberAsString = "";

  while (ptr) {
    numberAsString += `${ptr.val}`;
    ptr = ptr.next;
  }

  return parseInt(numberAsString, 10) || 0;
}

function numberToList(num) {
  if (num === 0) {
    return new ListNode(0, null);
  }

  let ptr = null;

  while (num) {
    const digit = num % 10;

    ptr = new ListNode(digit, ptr);

    num = Math.floor(num / 10);
  }

  return ptr;
}

/**
 * @param {ListNode} ptr1
 * @param {ListNode} ptr2
 * @return {ListNode}
 */
function addTwoNumbers(ptr1, ptr2) {
  const num1 = listToNumber(ptr1);
  const num2 = listToNumber(ptr2);
  const sum = num1 + num2;

  return numberToList(sum);
}

module.exports = addTwoNumbers;
