const ListNode = require("../shared/ListNode");

function listToArray(ptr) {
  const arr = [];

  while (ptr) {
    arr.push(ptr.val);
    ptr = ptr.next;
  }

  return arr;
}

/**
 * @param {ListNode} ptr1
 * @param {ListNode} ptr2
 * @return {ListNode}
 */
function addTwoNumbers(ptr1, ptr2) {
  const arr1 = listToArray(ptr1);
  const arr2 = listToArray(ptr2);

  let resultPtr = null;
  let savedDigit = 0;

  while (true) {
    const isArr1Empty = arr1.length === 0;
    const isArr2Empty = arr2.length === 0;

    if (isArr1Empty && isArr2Empty) {
      if (savedDigit) {
        resultPtr = new ListNode(savedDigit, resultPtr);
      }
      break;
    }

    const val1 = isArr1Empty ? 0 : arr1.pop();
    const val2 = isArr2Empty ? 0 : arr2.pop();
    const sum = val1 + val2 + savedDigit;
    const digit = sum % 10;
    savedDigit = sum >= 10 ? 1 : 0;

    resultPtr = new ListNode(digit, resultPtr);
  }

  return resultPtr;
}

module.exports = addTwoNumbers;
