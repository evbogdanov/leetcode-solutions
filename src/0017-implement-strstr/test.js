const solution1 = require("./solution1");
const solution2 = require("./solution2");

const INPUT_OUTPUT = [
  { input: ["hello", "ll"], output: 2 },
  { input: ["aaaaa", "bba"], output: -1 },
  { input: ["", ""], output: 0 },
];

describe("Implement strStr()", () => {
  const testSolution = (solution) => {
    for (const { input, output } of INPUT_OUTPUT) {
      expect(solution.apply(null, input)).toBe(output);
    }
  };

  test("Solution1", () => {
    testSolution(solution1);
  });

  test("Solution2", () => {
    testSolution(solution2);
  });
});
