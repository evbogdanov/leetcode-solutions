const ListNode = require("../shared/ListNode");

const solution1 = require("./solution1");
const solution2 = require("./solution2");
const solution3 = require("./solution3");
const solution4 = require("./solution4");

describe("Reverse Linked List", () => {
  let listHead = null;

  beforeEach(() => {
    const listNode3 = new ListNode(3, null);
    const listNode2 = new ListNode(2, listNode3);
    listHead = new ListNode(1, listNode2);
  });

  const checkReversed = (reversed) => {
    expect(reversed.val).toBe(3);
    expect(reversed.next.val).toBe(2);
    expect(reversed.next.next.val).toBe(1);
    expect(reversed.next.next.next).toBeNull();
  };

  test("First solution", () => {
    const reversed = solution1(listHead);
    checkReversed(reversed);
  });

  test("Second solution", () => {
    const reversed = solution2(listHead);
    checkReversed(reversed);
  });

  test("Third solution", () => {
    const reversed = solution3(listHead);
    checkReversed(reversed);
  });

  test("Fourth solution", () => {
    const reversed = solution4(listHead);
    checkReversed(reversed);
  });
});
