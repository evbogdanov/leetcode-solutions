/**
 * @param {ListNode} head
 * @return {ListNode}
 */
function reverseList(head) {
  let current = head;

  // Reversed nodes will be collected here
  let reversed = null;

  while (current !== null) {
    // Old link to a tail of the current node
    const savedTail = current.next;

    // New link to already reversed nodes
    current.next = reversed;

    // Every node up to the current one is reversed
    reversed = current;

    // Keep traversing the list
    current = savedTail;
  }

  return reversed;
}

module.exports = reverseList;
