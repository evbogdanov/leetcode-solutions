/**
 * @param {ListNode} head
 * @return {ListNode}
 */
function reverseList(head) {
  return rev(head, null);
}

function rev(current, reversed) {
  if (current === null) return reversed;

  const tail = current.next;

  current.next = reversed;

  return rev(tail, current);
}

module.exports = reverseList;
