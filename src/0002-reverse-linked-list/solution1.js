const ListNode = require("../shared/ListNode");

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
function reverseList(head) {
  let current = head;
  let reversed = null; // will be the last node

  while (current !== null) {
    reversed = new ListNode(current.val, reversed);
    current = current.next;
  }

  return reversed;
}

module.exports = reverseList;
