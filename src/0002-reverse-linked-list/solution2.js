const ListNode = require("../shared/ListNode");

/**
 * @param {ListNode} head
 * @return {ListNode}
 */
function reverseList(head) {
  return reverse(head, null);
}

/**
 * @param {?ListNode} head
 * @param {?ListNode} acc
 * @return {ListNode}
 */
function reverse(head, acc) {
  if (head === null) return acc;

  return reverse(head.next, new ListNode(head.val, acc));
}

module.exports = reverseList;
