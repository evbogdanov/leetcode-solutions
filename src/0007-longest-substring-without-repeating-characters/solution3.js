/**
 * @param {string} str
 * @return {number}
 */
function lengthOfLongestSubstring(str) {
  let longestLength = 0;

  const wind = new Map();

  for (let i = 0; i < str.length; i += 1) {
    const ch = str[i];
    const indexOfCh = wind.get(ch);

    if (indexOfCh === undefined) {
      wind.set(ch, i);
      longestLength = Math.max(longestLength, wind.size);
      continue;
    }

    wind.forEach((j, c, w) => {
      if (c === ch) {
        w.set(c, i);
      } else if (j < indexOfCh) {
        w.delete(c);
      }
    });
  }

  return longestLength;
}

module.exports = lengthOfLongestSubstring;
