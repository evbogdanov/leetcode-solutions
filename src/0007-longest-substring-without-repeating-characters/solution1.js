/**
 * @param {string} str
 * @return {number}
 */
function lengthOfLongestSubstring(str) {
  let longest = "";
  let candidates = [];

  for (let i = 0; i < str.length; i += 1) {
    const ch = str[i];
    if (longest === "") {
      longest = ch;
    }

    const nextCandidates = [];

    for (let candidate of candidates) {
      const hasChar = candidate.includes(ch);

      if (!hasChar) {
        candidate += ch;
        nextCandidates.push(candidate);
      }

      if (candidate.length > longest.length) {
        longest = candidate;
      }
    }

    nextCandidates.push(ch);
    candidates = nextCandidates;
  }

  return longest.length;
}

module.exports = lengthOfLongestSubstring;
