/**
 * @param {string} str
 * @return {number}
 */
function lengthOfLongestSubstring(str) {
  let longestLength = 0;

  const seenChars = new Map();

  for (let i = 0, j = 0; j < str.length; j += 1) {
    const ch = str[j];
    const indexOfCh = seenChars.get(ch);

    if (
      // Unseen character
      indexOfCh === undefined ||
      // Seen character. But it was in previous window.
      indexOfCh < i
    ) {
      longestLength = Math.max(longestLength, j - i + 1);
    } else {
      // Seen character in current window
      i = indexOfCh + 1;
    }

    seenChars.set(ch, j);
  }

  return longestLength;
}

module.exports = lengthOfLongestSubstring;
