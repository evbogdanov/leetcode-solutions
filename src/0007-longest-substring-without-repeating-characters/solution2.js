/**
 * @param {string} str
 * @return {number}
 */
function lengthOfLongestSubstring(str) {
  let longestLength = 0;

  // My sliding window
  let wind = "";

  // Two pointers
  // 1. `[` <-- `i` - start of the sliding window
  // 2. `]` <-- `j` - end of the sliding window
  // [a]vadakedavra
  // [av]adakedavra
  // a[va]dakedavra
  // a[vad]akedavra
  // ava[da]kedavra
  // ava[dak]edavra
  // ava[dake]davra
  // avad[aked]avra
  // avada[keda]vra
  // avada[kedav]ra
  // avada[kedavr]a
  // avadakeda[vra]
  for (let i = 0, j = 0; j < str.length; j += 1) {
    const ch = str[j];
    const indexOfCh = wind.indexOf(ch);

    // Current char is not in the window.
    // Grow the window to the right
    if (indexOfCh === -1) {
      wind += ch;

      if (wind.length > longestLength) longestLength = wind.length;

      continue;
    }

    // Shrink window from the left
    i = i + indexOfCh + 1;
    wind = str.slice(i, j + 1);
  }

  return longestLength;
}

module.exports = lengthOfLongestSubstring;
