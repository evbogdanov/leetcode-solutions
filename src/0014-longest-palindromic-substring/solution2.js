/**
 * @param {string} str
 * @return {string}
 */
function longestPalindrome(str) {
  let longest = "";

  for (let i = 0; i < str.length; i += 1) {
    let substr = "";
    let revSubstr = "";

    for (let j = i; j < str.length; j += 1) {
      substr += str[j];
      revSubstr = str[j] + revSubstr;
      if (substr.length > longest.length && substr === revSubstr) {
        longest = substr;
      }
    }
  }

  return longest;
}

module.exports = longestPalindrome;
