/**
 * @param {string} str
 * @return {string}
 */
function longestPalindrome(str) {
  let longest = "";

  // First scenario: palindrome with an odd number of characters (e.g. "madam")
  // [m] a  d  a  m
  //  m [a] d  a  m
  //  m  a [d] a  m <-- try to build a palindrome from every possible middle by expanding in both directions
  //  m  a  d [a] m
  //  m  a  d  a [m]
  for (let i = 0; i < str.length; i += 1) {
    let substr = "";
    let isPalindromePossible = true;
    for (
      let left = i, right = i;
      left >= 0 && right < str.length;
      left -= 1, right += 1
    ) {
      if (left === right) {
        substr = str[left];
      } else if (str[left] === str[right]) {
        substr = str[left] + substr + str[right];
      } else {
        isPalindromePossible = false;
      }

      if (substr.length > longest.length) {
        longest = substr;
      }

      if (!isPalindromePossible) break;
    }
  }

  // Second scenario: palindrome with an even number of characters (e.g. "abba")
  // [a] [b]  b   a  <-- middle is beetween two characters (left == 0, right == 1)
  //  a  [b] [b]  a
  //  a   b  [b] [a]
  for (let i = 0; i < str.length; i += 1) {
    let substr = "";
    for (
      let left = i, right = i + 1;
      left >= 0 && right < str.length;
      left -= 1, right += 1
    ) {
      if (str[left] !== str[right]) break;

      substr = str[left] + substr + str[right];
      if (substr.length > longest.length) {
        longest = substr;
      }
    }
  }

  return longest;
}

module.exports = longestPalindrome;
