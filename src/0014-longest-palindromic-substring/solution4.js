// This is a Dynamic Programming solution. It uses 2D array as a memo.
// The idea is simple:
// - Save in `memo` info about one and two letters palindromes
// - Then apply formula:
//   isPalindrome(left, right) === isPalindrome(left + 1, right - 1)
//                                 && str[left] === str[right]
// - `isPalindrome` is actually replaced by `memo`

/**
 * @param {string} str
 * @return {string}
 */
function longestPalindrome(str) {
  let longest = "";
  const memo = new Array(str.length)
    .fill([])
    .map((_) => new Array(str.length).fill(false));

  // Here I iterate over all possible substrings, starting from one letters
  // Substrings for "abc":
  // a
  // b
  // c
  // ab
  // bc // <-- initial memo is complete
  // abc
  for (let i = 0; i < str.length; i += 1) {
    for (let j = 0; i + j < str.length; j += 1) {
      const len = i + 1;

      if (len === 1) {
        if (j === 0) longest = str[0];
        memo[j][j] = true;
      } else if (len === 2) {
        const isPalindrome = str[j] === str[j + 1];
        memo[j][j + 1] = isPalindrome;
        if (isPalindrome && len > longest.length) {
          longest = str.slice(j, j + 2);
        }
      } else {
        const isPalindrome =
          str[j] === str[i + j] && memo[j + 1][j + i - 1] === true;
        memo[j][j + i] = isPalindrome;
        if (isPalindrome && len > longest.length) {
          longest = str.slice(j, j + i + 1);
        }
      }
    }
  }

  return longest;
}

module.exports = longestPalindrome;
