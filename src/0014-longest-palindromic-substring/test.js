const solution1 = require("./solution1");
const solution2 = require("./solution2");
const solution3 = require("./solution3");
const solution4 = require("./solution4");

const INPUT_OUTPUT = [
  ["", ""],
  ["a", "a"],
  ["ab", "a"],
  ["abbc", "bb"],
  ["abcbd", "bcb"],
  ["babad", "bab"],
  ["abba", "abba"],
  ["madam", "madam"],
];

describe("Longest Palindromic Substring", () => {
  const testSolution = (solution) => {
    for (const [input, output] of INPUT_OUTPUT) {
      expect(solution(input)).toBe(output);
    }
  };

  test("Solution1", () => {
    testSolution(solution1);
  });

  test("Solution2", () => {
    testSolution(solution2);
  });

  test("Solution3", () => {
    testSolution(solution3);
  });

  test("Solution4", () => {
    testSolution(solution4);
  });
});
