function isPalindrome(str) {
  for (let i = 0, j = str.length - 1; i < j; i += 1, j -= 1) {
    if (str[i] !== str[j]) return false;
  }

  return true;
}

/**
 * @param {string} str
 * @return {string}
 */
function longestPalindrome(str) {
  let longest = "";

  for (let i = 0; i < str.length; i += 1) {
    let substr = "";
    for (let j = i; j < str.length; j += 1) {
      substr += str[j];
      if (substr.length > longest.length && isPalindrome(substr)) {
        longest = substr;
      }
    }
  }

  return longest;
}

module.exports = longestPalindrome;
