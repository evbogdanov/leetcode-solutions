const INT32_MIN = -(2 ** 31);
const INT32_MAX = 2 ** 31 - 1;

module.exports = {
  INT32_MIN,
  INT32_MAX,
};
