function maxProfit(prices) {
  const mins = [];
  const maxs = [];

  let min = null;
  for (const price of prices) {
    if (min === null) {
      min = price;
    } else {
      min = Math.min(min, price);
    }

    mins.push(min);
  }

  let max = null;
  for (i = prices.length - 1; i >= 0; i -= 1) {
    const price = prices[i];

    if (max === null) {
      max = price;
    } else {
      max = Math.max(max, price);
    }

    maxs.unshift(max);
  }

  let maxProf = 0;
  for (let i = 0; i < mins.length; i += 1) {
    const min = mins[i];
    const max = maxs[i];
    const diff = max - min;

    maxProf = Math.max(maxProf, diff);
  }

  return maxProf;
}

module.exports = maxProfit;
