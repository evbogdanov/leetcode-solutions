function maxProfit(prices) {
  let maxProf = 0;
  let min = null;
  let max = null;

  for (const price of prices) {
    // Handle first price (which becomes initial min)
    if (min === null) {
      min = price;
    }
    // Handle new min
    else if (price < min) {
      min = price;
      max = null; // For this min there's no max seen (yet)
    }
    // Handle max
    else if (max === null || price > max) {
      max = price;
      maxProf = Math.max(maxProf, max - min);
    }
  }

  return maxProf;
}

module.exports = maxProfit;
