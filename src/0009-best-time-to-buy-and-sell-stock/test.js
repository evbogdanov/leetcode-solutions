const solution1 = require("./solution1");
const solution2 = require("./solution2");

const INPUT_OUTPUT = [
  [[], 0],
  [[1], 0],
  [[1, 1], 0],
  [[1, 2], 1],
  [[1, 2, 3], 2],
  [[3, 2, 1], 0],
  [[2, 1, 2, 0, 1], 1],
  [[7, 1, 5, 3, 6, 4], 5],
];

describe("Best Time to Buy and Sell Stock", () => {
  const testSolution = (solution) => {
    for (const [input, output] of INPUT_OUTPUT) {
      expect(solution(input)).toBe(output);
    }
  };

  test("Solution1", () => {
    testSolution(solution1);
  });

  test("Solution2", () => {
    testSolution(solution2);
  });
});
