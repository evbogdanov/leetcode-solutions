/**
 * @param {number[]} nums
 * @return {number}
 */
function removeDuplicates(nums) {
  let prevNum = null;

  for (let i = 0; i < nums.length; ) {
    if (nums[i] === prevNum) {
      nums.splice(i, 1);
    } else {
      prevNum = nums[i];
      i += 1;
    }
  }

  return nums.length;
}

module.exports = removeDuplicates;
