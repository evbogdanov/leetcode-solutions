const solution = require("./solution");

describe("Remove Duplicates from Sorted Array", () => {
  test("Empty array", () => {
    const nums = [];
    const len = solution(nums);

    expect(len).toBe(0);
    expect(nums).toEqual([]);
  });

  test("Not empty array", () => {
    const nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4];
    const len = solution(nums);

    const expected = [0, 1, 2, 3, 4];

    expect(len).toBe(expected.length);
    expect(nums).toEqual(expected);
  });
});
