const ROMAN_NUMS = {
  I: 1,
  V: 5,
  X: 10,
  L: 50,
  C: 100,
  D: 500,
  M: 1000,
  // Special Roman numbers
  IV: 4,
  IX: 9,
  XL: 40,
  XC: 90,
  CD: 400,
  CM: 900,
};

/**
 * @param {string} str
 * @return {number}
 */
function romanToInt(str) {
  let sum = 0;

  for (let i = 0; i < str.length; ) {
    const currentRoman = str[i];
    const nextRoman = str[i + 1];
    const specialRoman = `${currentRoman}${nextRoman}`;

    if (ROMAN_NUMS.hasOwnProperty(specialRoman)) {
      sum += ROMAN_NUMS[specialRoman];
      i += 2;
    } else {
      sum += ROMAN_NUMS[currentRoman];
      i += 1;
    }
  }

  return sum;
}

module.exports = romanToInt;
