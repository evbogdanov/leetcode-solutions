const solution = require("./solution");

const INPUT_OUTPUT = [
  ["", 0],
  ["I", 1],
  ["II", 2],
  ["III", 3],
  ["IV", 4],
  ["IX", 9],
  ["XIV", 14],
  ["XXIX", 29],
  ["LVIII", 58],
  ["MCMXCIV", 1994],
];

describe("Roman to Integer", () => {
  test("Solution", () => {
    for (const [input, output] of INPUT_OUTPUT) {
      expect(solution(input)).toBe(output);
    }
  });
});
