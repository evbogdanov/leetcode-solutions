const ListNode = require("../shared/ListNode");

const solution = require("./solution");

describe("Merge Two Sorted Lists", () => {
  test("Merge two empty lists", () => {
    const merged = solution(null, null);

    expect(merged).toBeNull();
  });

  test("Merge empty list with non-empty list", () => {
    const list = new ListNode(1, null);
    const merged = solution(null, list);

    expect(merged.val).toBe(1);
    expect(merged.next).toBeNull();
  });

  test("Merge two non-empty lists", () => {
    const list1Node3 = new ListNode(3, null);
    const list1Node2 = new ListNode(2, list1Node3);
    const list1 = new ListNode(1, list1Node2);

    const list2Node2 = new ListNode(4, null);
    const list2 = new ListNode(2, list2Node2);

    const merged = solution(list1, list2);

    expect(merged.val).toBe(1);
    expect(merged.next.val).toBe(2);
    expect(merged.next.next.val).toBe(2);
    expect(merged.next.next.next.val).toBe(3);
    expect(merged.next.next.next.next.val).toBe(4);
    expect(merged.next.next.next.next.next).toBeNull();
  });
});
