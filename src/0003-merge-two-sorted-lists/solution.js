const ListNode = require("../shared/ListNode");

/**
 * @param {ListNode} list1
 * @param {ListNode} list2
 * @return {ListNode}
 */
function mergeTwoLists(ptr1, ptr2) {
  // Result consists of two pointers:
  // 1. Head of the resulting list (it's a dummy)
  const resultPtrStart = new ListNode("dummy", null);

  // 2. Last element (to which new nodes will be added)
  let resultPtrEnd = resultPtrStart;

  while (true) {
    const isList1Traversed = ptr1 === null;
    const isList2Traversed = ptr2 === null;

    if (isList1Traversed && isList2Traversed) break;

    // Both lists have remaining nodes
    if (!isList1Traversed && !isList2Traversed) {
      if (ptr1.val < ptr2.val) {
        resultPtrEnd.next = ptr1;
        resultPtrEnd = ptr1;
        ptr1 = ptr1.next;
      } else {
        resultPtrEnd.next = ptr2;
        resultPtrEnd = ptr2;
        ptr2 = ptr2.next;
      }
    }
    // Only one of two lists still has nodes. Add them all to the result.
    else if (!isList1Traversed) {
      resultPtrEnd.next = ptr1;
      break;
    } else {
      resultPtrEnd.next = ptr2;
      break;
    }
  }

  return resultPtrStart.next;
}

module.exports = mergeTwoLists;
